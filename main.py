from account.account import Account
from account.current_account import CurrentAccount
from account.saving_account import SavingAccount
from users.client import Client
from users.functionary import Functionary
from users.general_manager import GeneralManager
from users.manager import Manager
from users.person import Person


def start():
    client = Client("0", "Gabriel", "05/08/1999")
    functionary = Functionary("1", "Lucas", "09/10/2000")
    manager = Manager("2", "Marcos", "09/08/1965")
    general_manager = GeneralManager("3", "Francisco", "008/06/1994")

    accounts = setup_accounts(client, functionary, manager, general_manager)

    withdraw_accounts(accounts)
    show_rates_applied(accounts)


def setup_accounts(*people):
    accounts = []
    counter = 0
    for person in people:
        if isinstance(person, Person):
            current_account = CurrentAccount(counter, person, 0.0)
            current_account.deposit(1000)

            saving_account = SavingAccount(counter, person, 0.0)
            saving_account.deposit(1000)

            accounts.append(current_account)
            accounts.append(saving_account)

            counter += 1
        else:
            continue
    return accounts


def withdraw_accounts(accounts):
    for account in accounts:
        if isinstance(account, Account):
            account.withdraw(100)


def show_rates_applied(accounts):
    print("Saving Accounts: ")

    for account in accounts:
        if isinstance(account, SavingAccount):
            print(f'Name: {account.person.name} Balance: {account.balance}')

    print("Current Accounts: ")

    for account in accounts:
        if isinstance(account, CurrentAccount):
            print(f'Name: {account.person.name} Balance: {account.balance}')

start()
