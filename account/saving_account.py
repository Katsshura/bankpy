from account.account import Account


class SavingAccount(Account):
    def __init__(self, id, person, balance):
        super().__init__(id, person, balance)

    def withdraw(self, value):
        discount_percent = self.get_rate() - super().person.get_current_account_discount_percent()
        withdraw_value = (value * discount_percent) + value
        super().withdraw(withdraw_value)

    def get_rate(self):
        return 0.01
