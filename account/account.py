from account.IAccount import IAccount


class Account(IAccount):
    def __init__(self, id, person, balance):
        self.__id = id
        self.__person = person
        self.__balance = balance

    @property
    def person(self):
        return self.__person

    @property
    def balance(self):
        return self.__balance

    def withdraw(self, value):
        if value <= self.__balance:
            self.__balance -= value
        else:
            print("You don't have sufficient balance")

    def deposit(self, value):
        if value <= 0:
            print("It's not possible deposit negative values")
        else:
            self.__balance += value
