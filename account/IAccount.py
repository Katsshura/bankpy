class IAccount:
    def withdraw(self, value):
        raise NotImplementedError

    def deposit(self, value):
        raise NotImplementedError

    def get_rate(self):
        raise NotImplementedError
