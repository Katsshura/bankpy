from users.person import Person


class Client(Person):
    def __init__(self, cpf, name, birthday):
        super().__init__(cpf, name, birthday)

    def get_current_account_discount_percent(self):
        return 0.0

    def get_saving_account_discount_percent(self):
        return 0.0
