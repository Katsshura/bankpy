from users.person import Person


class Functionary(Person):
    def __init__(self, cpf, name, birthday):
        super().__init__(cpf, name, birthday)

    def get_current_account_discount_percent(self):
        return 0.001

    def get_saving_account_discount_percent(self):
        return 0.003
