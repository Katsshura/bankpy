class IPerson:
    def get_current_account_discount_percent(self):
        raise NotImplementedError

    def get_saving_account_discount_percent(self):
        raise NotImplementedError
