from users.manager import Manager


class GeneralManager(Manager):
    def __init__(self, cpf, name, birthday):
        super().__init__(cpf, name, birthday)

    def get_current_account_discount_percent(self):
        return 0.005

    def get_saving_account_discount_percent(self):
        return 0.01
